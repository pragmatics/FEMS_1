package com.firebird.quartz.web.controller;

import com.firebird.quartz.domain.FirebirdQzScheduler;
import com.firebird.quartz.domain.FirebirdQzTask;
import com.firebird.quartz.server.DemoJobExecutionContext;
import com.firebird.quartz.server.FirebirdQzSchedulerManagerInter;
import com.firebird.quartz.server.impl.QuartzImpl;
import com.firebird.web.WebUtils;
import com.firebird.web.easyui.EasyUiUtils;
import com.firebird.web.log.FekpLog;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.firebird.core.page.OperateType;
import com.firebird.core.page.RequestMode;
import com.firebird.core.page.ViewMode;
import com.firebird.core.sql.query.DataQuery;
import com.firebird.core.sql.result.DataResult;

/**
 * 计划任务管理
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/qzScheduler")
@Controller
public class ActionFirebirdQzSchedulerQuery extends WebUtils {
	private static final Logger log = Logger.getLogger(ActionFirebirdQzSchedulerQuery.class);
	@Resource
	FirebirdQzSchedulerManagerInter firebirdQzSchedulerManagerImpl;

	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		query = EasyUiUtils.formatGridQuery(request, query);
		try {
			query = firebirdQzSchedulerManagerImpl.createSchedulerSimpleQuery(query);
			DataResult result = query.search().runDictionary("1:自动,0:手动", "AUTOIS");
			for (Map<String, Object> node : result.getResultList()) {
				try {
					node.put("RUN", firebirdQzSchedulerManagerImpl.isRunningFindScheduler(node.get("ID").toString()));
					node.put("RUNTYPE", node.get("RUN"));
				} catch (Exception e) {
					log.error(e);
				}
			}
			result.runDictionary("true:是,false:否", "RUN");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/chooseTask")
	public ModelAndView chooseTask(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("quartz/pFirebirdQzTaskGrid");
	}

	@RequestMapping("/chooseTrigger")
	public ModelAndView chooseTrigger(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("quartz/pFirebirdQzTriggerGrid");
	}

	@RequestMapping("/list")
	public ModelAndView list(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("quartz/pFirebirdQzSchedulerLayout");
	}

	/**
	 * start
	 * 
	 * @return
	 */
	@RequestMapping("/start")
	@ResponseBody
	public Map<String, Object> start(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				firebirdQzSchedulerManagerImpl.startTask(id);
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * stop
	 * 
	 * @return
	 */
	@RequestMapping("/stop")
	@ResponseBody
	public Map<String, Object> stop(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				firebirdQzSchedulerManagerImpl.stopTask(id);
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * runOnec
	 * 
	 * @return
	 */
	@RequestMapping("/runOnec")
	@ResponseBody
	public Map<String, Object> runOnec(final String ids, HttpSession session) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					for (String id : parseIds(ids)) {
						FirebirdQzTask task = firebirdQzSchedulerManagerImpl.getTaskBySchedulerId(id);
						if (task != null) {
							try {
								JobDetail jobDet = new QuartzImpl().getJobDetail("none", task);
								Job job = (Job) Class.forName(jobDet.getJobClass().getName()).newInstance();
								JobExecutionContext jobContext = new DemoJobExecutionContext(jobDet.getJobDataMap());
								job.execute(jobContext);
								FekpLog.info("定时任务执行完毕! " + task.getName(), "定时任务", "AUTO-RUN");
							} catch (Exception e) {
								log.error(e.toString(), e);
								FekpLog.error("执行定时任务" + task.getName() + ":" + e.getMessage(), "定时任务", "AUTO-RUN");
							}
						} else {
							log.warn("定时任务ID:" + id + "不存在!");
						}
					}
				}
			});
			t.start();
			return ViewMode.getInstance().putAttr("INFO", parseIds(ids).size() + "条任务被启动,执行情况请在日志中查看").returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/addSubmit")
	@ResponseBody
	public Map<String, Object> addSubmit(FirebirdQzScheduler entity, HttpSession session) {
		try {
			entity = firebirdQzSchedulerManagerImpl.insertSchedulerEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/editSubmit")
	@ResponseBody
	public Map<String, Object> editSubmit(FirebirdQzScheduler entity, HttpSession session) {
		try {
			entity = firebirdQzSchedulerManagerImpl.editSchedulerEntity(entity, getCurrentUser(session));

			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				firebirdQzSchedulerManagerImpl.deleteSchedulerEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.returnModelAndView("quartz/pFirebirdQzSchedulerEntity");
			}
			case (0): {// 展示
				FirebirdQzScheduler entity = firebirdQzSchedulerManagerImpl.getSchedulerEntity(ids);
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", entity)
						.putAttr("taskStr", firebirdQzSchedulerManagerImpl.getTaskEntity(entity.getTaskid()).getName())
						.putAttr("triggerStr",
								firebirdQzSchedulerManagerImpl.getTriggerEntity(entity.getTriggerid()).getName())
						.returnModelAndView("quartz/pFirebirdQzSchedulerEntity");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", firebirdQzSchedulerManagerImpl.getSchedulerEntity(ids))
						.returnModelAndView("quartz/pFirebirdQzSchedulerEntity");
			}
			default:
				break;
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("quartz/pFirebirdQzSchedulerEntity");
		}
		return ViewMode.getInstance().returnModelAndView("quartz/pFirebirdQzSchedulerEntity");
	}
}
