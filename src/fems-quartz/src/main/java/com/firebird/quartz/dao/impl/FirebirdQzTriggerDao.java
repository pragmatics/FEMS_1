package com.firebird.quartz.dao.impl;

import java.math.BigInteger;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.firebird.quartz.dao.FirebirdQzTriggerDaoInter;
import com.firebird.quartz.domain.FirebirdQzTrigger;
import com.firebird.core.sql.query.DBRule;
import com.firebird.core.sql.query.DataQuery;
import com.firebird.core.sql.result.DataResult;
import com.firebird.core.sql.utils.HibernateSQLTools;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

/**
 * 触发器定义
 * 
 * @author MAC_wd
 * 
 */
@Repository
public class FirebirdQzTriggerDao extends HibernateSQLTools<FirebirdQzTrigger>
		implements FirebirdQzTriggerDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	public void deleteEntity(FirebirdQzTrigger entity) {
		Session session = sessionFatory.getCurrentSession();
		session.delete(entity);
	}

	public int getAllListNum() {
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session
				.createSQLQuery("select count(*) from firebird_qz_trigger");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	public FirebirdQzTrigger getEntity(String id) {
		Session session = sessionFatory.getCurrentSession();
		return (FirebirdQzTrigger) session.get(FirebirdQzTrigger.class, id);
	}

	public FirebirdQzTrigger insertEntity(FirebirdQzTrigger entity) {
		Session session = sessionFatory.getCurrentSession();
		session.save(entity);
		return entity;
	}

	public void editEntity(FirebirdQzTrigger entity) {
		Session session = sessionFatory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Session getSession() {
		return sessionFatory.getCurrentSession();
	}

	public DataResult runSqlQuery(DataQuery query) {
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<FirebirdQzTrigger> selectEntitys(List<DBRule> rules) {
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	protected Class<?> getTypeClass() {
		return FirebirdQzTrigger.class;
	}
}
