package com.firebird.quartz.adapter;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.firebird.quartz.server.FirebirdQzSchedulerManagerInter;
import com.firebird.util.spring.BeanFactory;
import com.firebird.web.task.ServletInitJobInter;

public class StartSysTask implements ServletInitJobInter {
	private static final Logger log = Logger.getLogger(StartSysTask.class);

	@Override
	public void execute(ServletContext context) {
		FirebirdQzSchedulerManagerInter aloneIMP = (FirebirdQzSchedulerManagerInter) BeanFactory
				.getBean("firebirdQzSchedulerManagerImpl");
		try {
			aloneIMP.start();
			log.info("started '任务调度'");
		} catch (Exception e) {
			log.error(e);
		}
	}

}
