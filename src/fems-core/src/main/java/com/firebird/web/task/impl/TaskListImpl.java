package com.firebird.web.task.impl;

import java.util.List;

import com.firebird.web.task.ServletInitJobInter;
import com.firebird.web.task.TaskListInter;

public class TaskListImpl implements TaskListInter {
	private List<ServletInitJobInter> tasks;

	@Override
	public List<ServletInitJobInter> getTasks() {
		return tasks;
	}

	public void setTasks(List<ServletInitJobInter> tasks) {
		this.tasks = tasks;
	}

}
