package com.firebird.util.cache;

/**
 * 缓存名称
 * 
 * @author macpl
 *
 */
public enum FirebirdCacheNames {
	/**
	 * 获得分类知识（用于知识访问页面的知识推送）
	 */
	TypeDocsForDocpage("fekp-typedocsfordocpage"),
	/**
	 * 获取未读的消息
	 */
	UnReadMessagesByUser("fekp-unreadmessagesbyuser"),
	/**
	 * 获得所有专题,可用根据状态和id进行筛选（用于前台展示）
	 */
	AllSpecial("fekp-allspecial"),
	
	/**
	 * 获得所有专题,可用根据状态和id进行筛选（用于前台展示）
	 */
	TypeDocs("fekp-typedocs"),
	/**
	 * 获得用户所有（被授权阅读的受限分类的分类的ID）（阅读） ///////受限分类是相对于未分配权限的分类（未分配权限的分类，所有人均可访问）
	 */
	UserReadTypeids("fekp-userreadtypeids"),
	/**
	 * 获取知识分类信息（带分类下知识数量）用于创建知识时候的
	 */
	TypesForWriteDoc("fekp-typesforwritedoc"),
	/**
	 * 获取问题分类信息（带分类下问题数量）用于创建问题时候的
	 */
	FqaTypesForWriteDoc("fekp-fqatypesforwritedoc"),
	/**
	 * 获取知识分类信息（带分类下知识数量）
	 */
	PopTypesForReadDoc("fekp-poptypesforreaddoc"),
	/**
	 *  获取问答分类信息（带分类下问答数量）
	 */
	PopFqaTypesForReadDoc("fekp-popfqatypesforreaddoc"),
	/**
	 * 获取公开的知识分类信息
	 */
	pubTypeids("fekp-pubtypeids"),
	/**
	 * 获得分类所有子节点（包含它自己）
	 */
	AllSubType("fekp-allsubnode"),
	/**
	 * 获取知识分类详细信息（带分类下知识数量）（下级分类和下下级别分类）
	 */
	TypeInfos("fekp-typeinfos"),
	/**
	 *  获取友情链接集合,所有的
	 */
	WebUrlList("fekp-weburllist"),
	/**
	 * 获取友情链接集合,不登录显示的
	 */
	WebUrlListNoLogin("fekp-weburllistnologin"),
	/**
	 * 获得某分类下的问答
	 */
	TypeFqas("fekp-typefqas"),
	/**
	 * 最新的公开知识的评论
	 */
	NewPublicDocMessages("fekp-newpublicdocmessages"),
	/**
	 * 最热已完成问答
	 */
	HotQuestionByFinish("fekp-hotquestionbyfinish"),
	/**
	 * 最热待完成问答
	 */
	HotQuestionByWaiting("fekp-hotquestionbywaiting"),
	/**
	 * 最热问答
	 */
	HotQuestion("fekp-hotquestion"),
	/**
	 *展示最新知识
	 */
	NewKnowList("fekp-newknowlist"),
	/**
	 * 获得最热知识
	 */
	PubHotDoc("fekp-pubhotdoc"),
	/**
	 *获得置顶知识
	 */
	PubTopdoc("fekp-pubtopdoc"),
	/**
	 *获得好评用户
	 */
	StatGoodUsers("fekp-statgoodusers"),
	/**
	 *获得好评小组
	 */
	StatGoodGroups("fekp-statgoodgroups"),
	/**
	 *最多知识用户
	 */
	StatMostUsers("fekp-statmostusers"),
	/**
	 *好评文档
	 */
	StatGoodDocs("fekp-statgooddocs"),
	/**
	 *差评文档
	 */
	StatBadDocs("fekp-statbaddocs"),
	/**
	 *整体用量(获得每天的知识总数、好评数、差评数)
	 */
	StatNumForDay("fekp-statnumforday"),
	/**
	 * 用户知识统计
	 */
	StatUser("fekp-statuser"),
	/**
	 * 获得最热门小组
	 */
	HotDocGroups("fekp-hotdocgroups");
	
	/**
	 * 持久缓存
	 */
	private String permanentCacheName;
	/**
	 * 动态缓存
	 */
	private String liveCacheName;

	FirebirdCacheNames(String permanentCacheName) {
		this.permanentCacheName = permanentCacheName;
		this.liveCacheName = permanentCacheName + "-live";
	}

	/**
	 * 如果只有一个缓存就是这个持久缓缓存
	 * 
	 * @return
	 */
	public String getPermanentCacheName() {
		return permanentCacheName;
	}

	/**
	 * 动态缓存，短时间的缓存
	 * 
	 * @return
	 */
	public String getLiveCacheName() {
		return liveCacheName;
	}

}
