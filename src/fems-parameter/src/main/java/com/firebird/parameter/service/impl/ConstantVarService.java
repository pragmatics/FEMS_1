package com.firebird.parameter.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.firebird.core.auth.util.DesUtil;
import com.firebird.core.config.ReadKey;
import com.firebird.core.time.TimeTool;
import com.firebird.parameter.util.InfoUtil;
import com.firebird.util.spring.HibernateSessionFactory;
import com.firebird.util.web.WaterCodeUtils;
import com.firebird.web.constant.FirebirdConstant;

@Service
public class ConstantVarService {
	private static Map<String, String> constant = new HashMap<String, String>();

	public static void registConstant(String key, String value) {
		constant.put(key, value);
		if (getValue("firebird.constant.webroot.path") != null) {
			ReadKey.read(getValue("firebird.constant.webroot.path"));
		}
	}

	public static List<Entry<String, String>> getEntrys() {
		List<Entry<String, String>> list = new ArrayList<Entry<String, String>>();
		for (Entry<String, String> node : constant.entrySet()) {
			list.add(node);
		}
		return list;
	}

	public static String getValue(String key) {
		return constant.get(key);
	}

	public static void registConstant(String str, ServletContext context) {
		context.setAttribute("licenceInfo", InfoUtil.getIpAddress());
		context.setAttribute(FirebirdConstant.LCENCE_AUTH_WEB_KEY, getLicenceAuth());
	}

	private static String getLicenceAuth() {
		return "OSCHINA";
	}
}
