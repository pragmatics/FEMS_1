package com.firebird.web.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import com.firebird.core.auth.domain.LoginUser;
import com.firebird.parameter.FirebirdParameterService;
import com.firebird.web.constant.FirebirdConstant;

public class ParameterTaget extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Logger log = Logger.getLogger(ParameterTaget.class);
	private String key;

	@Override
	public int doEndTag() throws JspException {
		return 0;
	}

	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) this.pageContext
				.getRequest();
		LoginUser currentUser = (LoginUser) request.getSession().getAttribute(
				FirebirdConstant.SESSION_USEROBJ);
		JspWriter jspw = this.pageContext.getOut();
		try {
			if (currentUser == null) {
				jspw.print(FirebirdParameterService.getInstance().getParameter(
						key.trim()));
			} else {
				jspw.print(FirebirdParameterService.getInstance().getParameter(
						key.trim(), currentUser.getId()));
			}

		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return 0;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
