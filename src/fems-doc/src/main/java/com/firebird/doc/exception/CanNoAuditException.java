package com.firebird.doc.exception;

import com.firebird.parameter.FirebirdParameterService;

/**
 * 没有删除权限异常
 * 
 * @author Administrator
 * 
 */
public class CanNoAuditException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CanNoAuditException(String message) {
		super(message);
	}

	public CanNoAuditException() {
		super("没有审核权限");
	}
}
