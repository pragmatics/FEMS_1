package com.firebird.doc.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.firebird.core.sql.query.DBRule;
import com.firebird.core.sql.query.DBRuleList;
import com.firebird.core.sql.query.DataQuery;
import com.firebird.core.sql.result.DataResult;
import com.firebird.core.sql.utils.HibernateSQLTools;
import com.firebird.doc.dao.FirebirdDocfileDaoInter;
import com.firebird.doc.domain.FirebirdDocfile;

/**
 * 文档附件
 * 
 * @author MAC_wd
 * 
 */
@Repository
public class FirebirdDocfileDao extends HibernateSQLTools<FirebirdDocfile>implements FirebirdDocfileDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	public void deleteEntity(FirebirdDocfile entity) {
		Session session = sessionFatory.getCurrentSession();
		session.delete(entity);
	}

	public int getAllListNum() {
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery("select count(*) from firebird_docfile");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	public FirebirdDocfile getEntity(String id) {
		Session session = sessionFatory.getCurrentSession();
		return (FirebirdDocfile) session.get(FirebirdDocfile.class, id);
	}

	public FirebirdDocfile insertEntity(FirebirdDocfile entity) {
		if (entity.getDownum() == null) {
			entity.setDownum(0);
		}
		Session session = sessionFatory.getCurrentSession();
		session.save(entity);
		return entity;
	}

	public void editEntity(FirebirdDocfile entity) {
		if (entity.getDownum() == null) {
			entity.setDownum(0);
		}
		Session session = sessionFatory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Session getSession() {
		return sessionFatory.getCurrentSession();
	}

	public DataResult runSqlQuery(DataQuery query) {
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<FirebirdDocfile> selectEntitys(List<DBRule> rules) {
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	public SessionFactory getSessionFatory() {
		return sessionFatory;
	}

	public void setSessionFatory(SessionFactory sessionFatory) {
		this.sessionFatory = sessionFatory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FirebirdDocfile> getDocFilesByDocId(String id) {
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery(
				"SELECT DISTINCT b.* FROM firebird_rf_doctextfile a LEFT JOIN firebird_docfile b ON a.FILEID=b.ID WHERE  PSTATE='1' AND a.DOCID=? order by b.name asc");
		sqlquery.setString(0, id);
		sqlquery.addEntity(FirebirdDocfile.class);
		return sqlquery.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FirebirdDocfile> getDocFilesByDocTextId(String textid) {
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery(
				"SELECT DISTINCT b.* FROM firebird_rf_doctextfile a LEFT JOIN firebird_docfile b ON a.FILEID=b.ID WHERE  PSTATE='1' AND a.TEXTID=? order by b.name asc");
		sqlquery.setString(0, textid);
		sqlquery.addEntity(FirebirdDocfile.class);
		return sqlquery.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FirebirdDocfile> getEntityByDocIdAndExName(String docid, String exname) {
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery(
				"SELECT DISTINCT b.* FROM firebird_rf_doctextfile a LEFT JOIN firebird_docfile b ON a.FILEID=b.ID WHERE  PSTATE='1' AND a.DOCID=? AND b.EXNAME=? order by b.etime desc");
		sqlquery.setString(0, docid);
		sqlquery.setString(1, exname);
		sqlquery.addEntity(FirebirdDocfile.class);
		return sqlquery.list();
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	protected Class<FirebirdDocfile> getTypeClass() {
		return FirebirdDocfile.class;
	}

	@Override
	public List<FirebirdDocfile> getfilesByAppids(List<String> appids) {
		List<FirebirdDocfile> files = new ArrayList<>();
		for (String appid : appids) {
			files.addAll(selectEntitys(DBRuleList.getInstance().add(new DBRule("APPID", appid, "=")).toList()));
		}
		return files;
	}

}
