package com.firebird.doc.util;

import com.firebird.parameter.FirebirdParameterService;

public class ThemesUtil {
	public static String getThemePath() {
		return FirebirdParameterService.getInstance().getParameter("config.sys.web.themes.path");
	}
}
