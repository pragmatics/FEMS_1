package com.firebird.web.init;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.firebird.parameter.FirebirdParameterService;
import com.firebird.parameter.service.impl.ConstantVarService;
import com.firebird.parameter.service.impl.PropertiesFileService;
import com.firebird.parameter.service.impl.XmlConfigFileService;
import com.firebird.fekp.util.ThemesUtil;
import com.firebird.web.constant.FirebirdConstant;
import com.firebird.web.task.ServletInitJobInter;

public class InitParameter implements ServletInitJobInter {
	private static final Logger log = Logger.getLogger(InitParameter.class);

	@Override
	public void execute(ServletContext context) {
		// 注册常量
		FirebirdParameterService.getInstance().getDictionary("123");//随便查点什么，为了初始化spring和hibernate
		ConstantVarService.registConstant("firebird.constant.session.key.logintime.", FirebirdConstant.SESSION_LOGINTIME);
		ConstantVarService.registConstant("firebird.constant.session.key.current.org", FirebirdConstant.SESSION_ORG);
		ConstantVarService.registConstant("firebird.constant.session.key.current.roles", FirebirdConstant.SESSION_ROLES);
		ConstantVarService.registConstant("firebird.constant.session.key.current.actions", FirebirdConstant.SESSION_USERACTION);
		ConstantVarService.registConstant("firebird.constant.session.key.current.menu", FirebirdConstant.SESSION_USERMENU);
		ConstantVarService.registConstant("firebird.constant.session.key.current.user", FirebirdConstant.SESSION_USEROBJ);
		ConstantVarService.registConstant("firebird.constant.session.key.current.userphoto",
				FirebirdConstant.SESSION_USERPHOTO);
		ConstantVarService.registConstant("firebird.constant.session.key.go.url", FirebirdConstant.SESSION_GO_URL);
		ConstantVarService.registConstant("firebird.constant.session.key.from.url", FirebirdConstant.SESSION_FROM_URL);
		ConstantVarService.registConstant("firebird.constant.app.treecodelen",
				String.valueOf(FirebirdConstant.MENU_TREECODE_UNIT_LENGTH));
		ConstantVarService.registConstant("firebird.constant.webroot.path", context.getRealPath(""));
		ConstantVarService.registConstant("firebird.constant.webroot.path", context);
		// 注册配置文件
		PropertiesFileService.registConstant("jdbc");
		PropertiesFileService.registConstant("config/config");
		PropertiesFileService.registConstant("config/about");
		// 注册xml配置文件
		XmlConfigFileService.registConstant("FekpWebConfig.xml");
		XmlConfigFileService.registConstant("FekpInterConfig.xml");
		sysConstant();
		checkXml();
		// 加载xml配置文件到系统变量中(必须在加载完配置文件后才有意义)
		XmlConfigFileService.loadXmlToApplication(context);
		// 加载风格配置文件
		ThemesUtil.loadXmlConf();
	}

	private static void sysConstant() {
		ConstantVarService.registConstant("firebird.sys.java.version","JDK"+ System.getProperty("java.version"));
		ConstantVarService.registConstant("firebird.sys.java.home", System.getProperty("java.home"));
		ConstantVarService.registConstant("firebird.sys.java.os.name", System.getProperty("os.name"));
		ConstantVarService.registConstant("firebird.sys.java.os.version", System.getProperty("os.version"));
		ConstantVarService.registConstant("firebird.sys.tomcat.dir", System.getProperty("catalina.base"));
		ConstantVarService.registConstant("firebird.sys.logs.dir", System.getProperty("catalina.base")+File.separator+"fekplogs");
	}

	/**
	 * 检测xml中参数是否完整
	 */
	private static void checkXml() {
		log.info("检查XML配置文件参数：------start-----------------------------------");
		List<String> names = XmlConfigFileService.readCheckXmlConfig("config/xmlChecks.xml");
		for (String name : names) {
			checkXmlParameter(name);
		}
		log.info("检查XML配置文件参数：------end-----------------------------------");
	}

	private static void checkXmlParameter(String key) {
		if (XmlConfigFileService.getValue(key) != null) {
			log.info("-----OK:" + key);
		} else {
			log.info("[ERROR]:" + key);
		}
	}

	public static void main(String[] args) {
		// 检查xml配置文件
		// XmlConfigFileService.registConstant("FekpWebConfig.xml");
		// XmlConfigFileService.registConstant("FekpInterConfig.xml");
		// checkXml();
		// System.out.println(AppConfig.getString("config.about"));
	}
}
