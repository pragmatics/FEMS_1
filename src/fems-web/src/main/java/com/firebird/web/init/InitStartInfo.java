package com.firebird.web.init;

import javax.servlet.ServletContext;

import com.firebird.core.config.AppConfig;
import com.firebird.web.task.ServletInitJobInter;

/**展示项目信息
 * @author macpl
 *
 */
public class InitStartInfo implements ServletInitJobInter {
	@Override
	public void execute(ServletContext context) {
		System.out.println("physics path:" + context.getRealPath(""));
		System.out.println("fems version:" + AppConfig.getString("config.sys.version"));
	}
}
