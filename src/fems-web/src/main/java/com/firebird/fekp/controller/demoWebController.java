package com.firebird.fekp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.firebird.authority.FirebirdAuthorityService;
import com.firebird.core.auth.domain.LoginUser;
import com.firebird.core.page.ViewMode;
import com.firebird.core.sql.query.DBSort;
import com.firebird.core.sql.query.DataQuery;
import com.firebird.core.sql.result.DataResult;
import com.firebird.core.sql.result.ResultsHandle;
import com.firebird.parameter.FirebirdParameterService;
import com.firebird.web.WebUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.fems.exam.domain.Paper;
import com.fems.exam.domain.Card;
import com.fems.exam.domain.Room;
import com.fems.exam.domain.SubjectAnalysis;
import com.fems.exam.domain.ex.PaperUnit;
import com.fems.exam.domain.ex.RoomUnit;
import com.fems.exam.domain.ex.SubjectUnit;
import com.fems.exam.service.ExamTypeServiceInter;
import com.fems.exam.service.MaterialServiceInter;
import com.fems.exam.service.PaperServiceInter;
import com.fems.exam.service.CardServiceInter;
import com.fems.exam.service.ExamPopsServiceInter;
import com.fems.exam.service.RoomServiceInter;
import com.fems.exam.service.SubjectAnalysisServiceInter;
import com.fems.exam.service.SubjectServiceInter;

/**
 * 判卷
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/demo")
@Controller
public class demoWebController extends WebUtils {

	public static String getThemePath() {
		return FirebirdParameterService.getInstance().getParameter("config.sys.web.themes.path");
	}

	/***
	 * 考场判卷首页
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubHome")
	public ModelAndView index(String roomid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			return view.returnModelAndView("/demo/index");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnModelAndView(getThemePath() + "/error");
		}
	}

}
