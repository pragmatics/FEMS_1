$(function(){
	//pc端
	$('.fems-hometype-ul li').click(function(){
		$(".fems-hometype-ul li").removeClass("fems-hometype-active");
		$(this).addClass("fems-hometype-active");
		$('.fems-subjects li').hide();
		$('#'+$(this).attr("targetId")).show();
	});
	//移动端
	$('.fems-mobile-hometype-ul li').click(function(){
		$('.fems-subjects li').hide();
		$('#'+$(this).attr("targetId")).show();
		$(".fems-mobile-hometype-ul li").removeClass("active");
		$(this).addClass("active");
	});
});