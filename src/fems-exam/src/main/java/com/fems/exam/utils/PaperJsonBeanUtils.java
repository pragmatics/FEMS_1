package com.fems.exam.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.fems.exam.domain.ex.FemsPaperBean;

public class PaperJsonBeanUtils {

	/**
	 * 将封装对象导出为对象
	 * 
	 * @param papaerFile
	 * @param paperj
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void writeToFile(File papaerFile, FemsPaperBean paperj) throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(papaerFile));
		try {
			oos.writeObject(paperj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		oos.close();
	}

	/**
	 * 从femsp文件中读取答卷封装对象
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static FemsPaperBean readFromFile(InputStream inputStream) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(inputStream);
		try {
			FemsPaperBean bean = (FemsPaperBean) ois.readObject();
			return bean;
		} finally {
			ois.close();
		}
	}

}
