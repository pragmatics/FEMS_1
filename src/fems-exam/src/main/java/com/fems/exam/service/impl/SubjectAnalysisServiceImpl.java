package com.fems.exam.service.impl;

import com.fems.exam.domain.SubjectAnalysis;
import com.firebird.core.time.TimeTool;
import com.firebird.doc.server.FirebirdFileManagerInter;
import com.firebird.doc.server.FirebirdFileManagerInter.FILE_APPLICATION_TYPE;

import org.apache.log4j.Logger;
import com.fems.exam.dao.SubjectAnalysisDaoInter;
import com.fems.exam.service.SubjectAnalysisServiceInter;
import com.firebird.core.sql.query.DBRule;
import com.firebird.core.sql.query.DBRuleList;
import com.firebird.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import javax.annotation.Resource;
import com.firebird.core.auth.domain.LoginUser;

/* *
 *功能：试题解析服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FirebirdCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class SubjectAnalysisServiceImpl implements SubjectAnalysisServiceInter {
	@Resource
	private SubjectAnalysisDaoInter SubjectAnalysisDaoImpl;
	@Resource
	private FirebirdFileManagerInter firebirdFileManagerImpl;
	private static final Logger log = Logger.getLogger(SubjectAnalysisServiceImpl.class);

	@Override
	@Transactional
	public SubjectAnalysis insertSubjectAnalysisEntity(SubjectAnalysis entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity = SubjectAnalysisDaoImpl.insertEntity(entity);
		firebirdFileManagerImpl.submitFileByAppHtml(entity.getText(), entity.getId(),
				FILE_APPLICATION_TYPE.SUBJECT_ANALYSIS);
		return entity;
	}

	@Override
	@Transactional
	public SubjectAnalysis editSubjectAnalysisEntity(SubjectAnalysis entity, LoginUser user) {
		SubjectAnalysis entity2 = SubjectAnalysisDaoImpl.getEntity(entity.getId());
		String oldText = entity2.getText();
		entity2.setText(entity.getText());
		entity2.setPcontent(entity.getPcontent());
		entity2.setPstate(entity.getPstate());
		SubjectAnalysisDaoImpl.editEntity(entity2);
		firebirdFileManagerImpl.updateFileByAppHtml(oldText, entity2.getText(), entity2.getId(),
				FILE_APPLICATION_TYPE.SUBJECT_ANALYSIS);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteSubjectAnalysisEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		SubjectAnalysisDaoImpl.deleteEntity(SubjectAnalysisDaoImpl.getEntity(id));
		firebirdFileManagerImpl.cancelFilesByApp(id);
	}

	@Override
	@Transactional
	public SubjectAnalysis getSubjectAnalysisEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return SubjectAnalysisDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createSubjectAnalysisSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "FEMS_SUBJECT_ANALYSIS",
				"ID,SUBJECTID,TEXT,PCONTENT,PSTATE,CUSERNAME,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<SubjectAnalysis> getSubjectAnalysies(String subjectid) {
		DBRuleList dblist = DBRuleList.getInstance().add(new DBRule("SUBJECTID", subjectid, "="));
		return SubjectAnalysisDaoImpl.selectEntitys(dblist.toList());
	}
}
